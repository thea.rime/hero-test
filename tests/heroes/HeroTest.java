package heroes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    @Test
    public void hero_correctName_shouldReturnName() {
        Hero mage = new Mage("Thea");
        String expectedName = "Thea";
        String actualName = mage.displayName();

        assertEquals(expectedName, actualName);

    }

    @Test
    public void hero_correctLevel_shouldReturnOne() {
        Hero mage = new Mage("Thea");
        int expectedLevel = 1;
        int actualLevel = mage.getLevel();

        assertEquals(expectedLevel, actualLevel);

    }



}