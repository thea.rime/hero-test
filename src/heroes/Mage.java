package heroes;

import equipments.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;

public class Mage extends Hero  {

    public String name;
    public Mage(String name){
        super(name);
        setStrength(1);
        setDexterity(1);
        setIntelligence(8);
    }

    @Override
    public int levelUp() {
        super.levelUp();
        int newStrength = getStrength() + 1;
        int newDexterity = getDexterity() + 1;
        int newIntelligence = getIntelligence() + 5;
        setStrength(newStrength);
        setDexterity(newDexterity);
        setIntelligence(newIntelligence);
        return newStrength + newDexterity + newIntelligence;
    }

    @Override
    public void equipWeapons(WeaponType type, String name, int requiredLevel) throws InvalidWeaponException {
        switch (type){
            case STAFFS, WANDS -> {
                setWeapon(Slot.WEAPON, new Weapon(name,type,requiredLevel));
            }
            default -> throw new InvalidWeaponException("Can not equip this weapon.");
        }

    }

    @Override
    public void equipArmor(ArmorType type, String name, int requiredLevel) throws InvalidArmorException {
        switch (type){
            case CLOTH -> {
                setArmor(Slot.BODY, new Armor(name, type, new HeroAttribute(1,2,3), requiredLevel));
            }
            default -> throw new InvalidArmorException("Can not equip this weapon.");
        }

    }

   // @Override
    //public void damage() {
     //   damage += totalIntelligence;
    //}

    //@Override
    //public void totalAttributes() {
      //  levelUp();
    //}
    public void display(){
        System.out.println("MAAAAGEE");
        super.display();
    }

}
