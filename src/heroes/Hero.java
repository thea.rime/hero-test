package heroes;

import equipments.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Hero extends HeroAttribute {

    private String name;

    private int level = 1;

    private int levelAttributes;


    private HashMap<Slot, Item> Equipment = new HashMap<Slot, Item>();
    private List<Object> ValidWeaponTypes = new ArrayList<>();
    private List<Object> ValidArmorTypes = new ArrayList<>();

    private HeroAttribute heroAttribute;

    public Hero(String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }


    public int levelUp() {
        level++;
    }


    // public static damage() {
    //     return WeaponDamage * (1+ DamagingAttribute/100);
    //}

    // public void totalAttributes() {
    //     levelUp() + ;
    //}

    public void display() {
        System.out.println(this.name);
        System.out.println(this.level);
        super.display();
    }

    public List<Object> getValidWeaponTypes() {
        return ValidWeaponTypes;
    }

    public void setValidWeaponTypes(List<Object> validWeaponTypes) {
        ValidWeaponTypes = validWeaponTypes;
    }

    public List<Object> getValidArmorTypes() {
        return ValidArmorTypes;
    }

    public void setValidArmorTypes(List<Object> validArmorTypes) {
        ValidArmorTypes = validArmorTypes;
    }

    //STRINGBUILDER
        /*
    public String toString() {
        return "Hero: " + name + '\'' + ", Class: " + ", Level: "
                + level + ", Total Strength: " + getStrength() +
                ", Total Dexterity: " + getDexterity() + ", Total " +
                "intelligence: " + getIntelligence() + ", Damage: " + damage();
    } */

    public void setWeapon(Slot key, Weapon value) {
        Equipment.put(key, value);
    }

    public void setArmor(Slot key, Armor value) {
        Equipment.put(key, value);
    }

    public int getLevel() {
        return level;
    }


    public void equipWeapons(WeaponType type, String name, int requiredLevel) throws InvalidWeaponException {

        throw new InvalidWeaponException("Can not equip weapon");
    }

    //public void equipArmor(ArmorType type, String name, int requiredLevel) throws InvalidArmorException {}

    //public abstract void damage();

    //public abstract void totalAttributes();

}
