package heroes;

import equipments.Slot;
import equipments.Weapon;
import equipments.WeaponType;
import exceptions.InvalidWeaponException;

public class Ranger   {

    public String name;

    public Ranger(String name){
        super(name);
        setStrength(1);
        setDexterity(7);
        setIntelligence(1);
    }

    @Override
    public void levelUp() {
        super.levelUp();
        int newStrength = getStrength() + 1;
        int newDexterity = getDexterity() + 5;
        int newIntelligence = getIntelligence() + 1;
        setStrength(newStrength);
        setDexterity(newDexterity);
        setIntelligence(newIntelligence);

    }

    @Override
    public void equipWeapons(WeaponType type, String name, int requiredLevel) throws InvalidWeaponException {
        switch (type){
            case BOWS -> {
                setWeapon(Slot.WEAPON, new Weapon(name,type,requiredLevel));
            }
            default -> throw new InvalidWeaponException("Can not equip this weapon.");
        }

    }

    @Override
    public void equipArmor() {

    }

    @Override
    public void damage() {
        damage += totalDexterity;

    }

    @Override
    public void totalAttributes() {

    }
}
