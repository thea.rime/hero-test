package heroes;

import equipments.Slot;
import equipments.Weapon;
import equipments.WeaponType;
import exceptions.InvalidWeaponException;

public class Warrior   {

    public String name;

    public Warrior(String name){
        super(name);
        setStrength(5);
        setDexterity(2);
        setIntelligence(1);}
    @Override
    public void levelUp() {
        super.levelUp();
        int newStrength = getStrength() + 3;
        int newDexterity = getDexterity() + 2;
        int newIntelligence = getIntelligence() + 1;
        setStrength(newStrength);
        setDexterity(newDexterity);
        setIntelligence(newIntelligence);

    }

    @Override
    public void equipWeapons(WeaponType type, String name, int requiredLevel) throws InvalidWeaponException {
        switch (type){
            case AXES, HAMMERS, SWORDS -> {
                setWeapon(Slot.WEAPON, new Weapon(name,type,requiredLevel));
            }
            default -> throw new InvalidWeaponException("Can not equip this weapon.");
        }

    }

    @Override
    public void equipArmor() {

    }

    @Override
    public void damage() {
        damage += totalStrength;

    }

    @Override
    public void totalAttributes() {

    }
}