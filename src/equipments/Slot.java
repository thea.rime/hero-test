package equipments;

public enum Slot {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
