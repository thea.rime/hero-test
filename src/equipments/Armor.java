package equipments;

import heroes.HeroAttribute;

public class Armor extends Item {

    private final ArmorType armorType;

    private HeroAttribute armorAttribute;

    public Armor(String name, ArmorType armorType, HeroAttribute armorAttribute, int requiredLevel){
        setName(name);
        this.armorType = armorType;
        this.armorAttribute = armorAttribute;
        setRequiredLevel(requiredLevel);

    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public int armorAttributes() {
        return armorAttribute.getDexterity() + armorAttribute.getIntelligence()
                + armorAttribute.getIntelligence();
    }
}
