package equipments;

public enum WeaponType {
    AXES,
    BOWS,
    DAGGERS,
    HAMMERS,
    STAFFS,
    SWORDS,
    WANDS
}
