package equipments;

public class Weapon extends Item {


    public int weaponDamage;

    private final WeaponType weaponType;

    public Weapon(String name, WeaponType weaponType, int requiredLevel){
        setName(name);
        this.weaponType = weaponType;
        setRequiredLevel(requiredLevel);
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }


}
