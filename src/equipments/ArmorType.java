package equipments;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
